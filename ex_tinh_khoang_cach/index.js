function tinh() {
  var kcNguyen = document.getElementById("txt-nguyen").value * 1;
  var kcVinh = document.getElementById("txt-vinh").value * 1;
  var kcNam = document.getElementById("txt-nam").value * 1;
  if (kcVinh > kcNam && kcVinh > kcNguyen) {
    document.getElementById(
      "result"
    ).innerHTML = ` Khoảng cách của Vinh xa nhất`;
  } else if (kcNguyen > kcVinh && kcNguyen > kcNam) {
    document.getElementById(
      "result"
    ).innerHTML = `Khoảng cách của Nguyên lớn nhất`;
  } else {
    document.getElementById(
      "result"
    ).innerHTML = `Khoảng cách của Nam lớn nhất`;
  }
}
