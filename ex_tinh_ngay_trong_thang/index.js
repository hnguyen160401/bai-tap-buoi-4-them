function tinh() {
  var thang = document.getElementById("txt-thang").value * 1;
  var nam = document.getElementById("txt-nam").value * 1;
  var ngay = null;
  if (nam % 400 == 0) {
    switch (thang) {
      case 1: {
        ngay = 31;
        break;
      }
      case 2: {
        ngay = 28;
        break;
      }
      case 3: {
        ngay = 31;
        break;
      }
      case 4: {
        ngay = 30;
        break;
      }
      case 5: {
        ngay = 31;
        break;
      }
      case 6: {
        ngay = 30;
        break;
      }
      case 7: {
        ngay = 31;
        break;
      }
      case 8: {
        ngay = 30;
        break;
      }
      case 9: {
        ngay = 31;
        break;
      }
      case 10: {
        ngay = 30;
        break;
      }
      case 11: {
        ngay = 31;
        break;
      }
      case 12: {
        ngay = 30;
        break;
      }
    }
  } else {
    switch (thang) {
      case 1: {
        ngay = 31;
        break;
      }
      case 2: {
        ngay = 28;
        break;
      }
      case 3: {
        ngay = 31;
        break;
      }
      case 4: {
        ngay = 30;
        break;
      }
      case 5: {
        ngay = 31;
        break;
      }
      case 6: {
        ngay = 30;
        break;
      }
      case 7: {
        ngay = 31;
        break;
      }
      case 8: {
        ngay = 30;
        break;
      }
      case 9: {
        ngay = 31;
        break;
      }
      case 10: {
        ngay = 30;
        break;
      }
      case 11: {
        ngay = 31;
        break;
      }
      case 12: {
        ngay = 30;
        break;
      }
    }
  }
  document.getElementById(
    "result"
  ).innerHTML = `Có ${ngay} ngày trong tháng ${thang} năm ${nam}`;
}
