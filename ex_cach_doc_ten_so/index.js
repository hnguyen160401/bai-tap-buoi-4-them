function doc() {
  var so = document.getElementById("txt-so").value * 1;
  var hangTram = Math.floor(so / 100);
  var hangChuc = Math.floor((so % 100) / 10);
  var hangDonVi = Math.floor(so % 10);
  var docHangTram = null;
  var docHangChuc = null;
  var docHangDonVi = null;
  //   console.log(hangTram, hangChuc, hangDonVi);
  switch (hangTram) {
    case 1: {
      docHangTram = "Một trăm";
    }
    case 2: {
      docHangTram = "Hai trăm";
    }
    case 3: {
      docHangTram = "Ba trăm";
    }
    case 4: {
      docHangTram = "Bốn trăm";
    }
    case 5: {
      docHangTram = "Năm trăm";
    }
    case 6: {
      docHangTram = "Sáu trăm";
    }
    case 7: {
      docHangTram = "Bảy trăm";
    }
    case 8: {
      docHangTram = "Tám trăm";
    }
    case 9: {
      docHangTram = "Chín trăm";
    }
  }
  switch (hangChuc) {
    case 1: {
      docHangChuc = "Một chục";
    }
    case 2: {
      docHangChuc = "Hai chục";
    }
    case 3: {
      docHangChuc = "Ba chục";
    }
    case 4: {
      docHangChuc = "Bốn chục";
    }
    case 5: {
      docHangChuc = "Năm chục";
    }
    case 6: {
      docHangChuc = "Sáu chục";
    }
    case 7: {
      docHangChuc = "Bảy chục";
    }
    case 8: {
      docHangChuc = "Tám chục";
    }
    case 9: {
      docHangChuc = "Chín chục";
    }
  }
  switch (hangDonVi) {
    case 1: {
      docHangDonVi = "Một nghìn";
    }
    case 2: {
      docHangDonVi = "Hai nghìn";
    }
    case 3: {
      docHangDonVi = "Ba nghìn";
    }
    case 4: {
      docHangDonVi = "Bốn nghìn";
    }
    case 5: {
      docHangDonVi = "Năm nghìn";
    }
    case 6: {
      docHangDonVi = "Sáu nghìn";
    }
    case 7: {
      docHangDonVi = "Bảy nghìn";
    }
    case 8: {
      docHangDonVi = "Tám nghìn";
    }
    case 9: {
      docHangDonVi = "Chín nghìn";
    }
  }
  document.getElementById(
    "result"
  ).innerHTML = `Cách đọc là : ${docHangTram} ${docHangChuc} ${docHangDonVi}`;
}
