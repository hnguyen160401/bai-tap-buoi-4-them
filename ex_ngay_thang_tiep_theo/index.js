function ngaySau() {
  var ngay = document.getElementById("txt-ngay").value * 1;
  var thang = document.getElementById("txt-thang").value * 1;
  var nam = document.getElementById("txt-nam").value * 1;

  if (ngay > 30) {
    thang++;
    ngay = 1;
  } else {
    ngay++;
  }

  if (thang > 12) {
    ngay = 1;
    thang = 1;
    nam++;
  }
  console.log(ngay);
  console.log(thang);
  console.log(nam);
  document.getElementById(
    "result"
  ).innerHTML = `ngày tiếp theo là ngày ${ngay} tháng ${thang} năm ${nam} `;
}

function ngayTruoc() {
  var ngay = document.getElementById("txt-ngay").value * 1;
  var thang = document.getElementById("txt-thang").value * 1;
  var nam = document.getElementById("txt-nam").value * 1;

  if (ngay < 2) {
    thang--;
    ngay = 31;
  } else {
    ngay--;
  }

  if (thang < 2) {
    ngay = 31;
    thang = 1;
    nam--;
  }
  console.log(ngay);
  console.log(thang);
  console.log(nam);
  document.getElementById(
    "result"
  ).innerHTML = `ngày tiếp theo là ngày ${ngay} tháng ${thang} năm ${nam} `;
}
